package kz.company.lesson_5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kz.company.lesson_5.ui.FragmentOne
import kz.company.lesson_5.ui.FragmentTwo

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentOneButton.setOnClickListener {
            supportFragmentManager
                .beginTransaction() // Начни транзацкию
                .replace(R.id.container, FragmentOne()) //Поменяй либо положи
                .addToBackStack(FragmentOne::class.java.simpleName)
                .commit() // завершение
        }

        fragmentTwoButton.setOnClickListener {
            supportFragmentManager
                .beginTransaction() // Начни транзацкию
                .replace(R.id.container, FragmentTwo()) //Поменяй либо положи
                .addToBackStack(FragmentTwo::class.java.simpleName)
                .commit() // завершение
        }
    }
}
