package kz.company.lesson_5.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_one.*
import kz.company.lesson_5.R

class FragmentOne : Fragment(R.layout.fragment_one) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button.setOnClickListener {
            activity?.let {
                it.supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, FragmentTwo())
                .commit()
            }
        }

        button.isVisible = true
    }
}